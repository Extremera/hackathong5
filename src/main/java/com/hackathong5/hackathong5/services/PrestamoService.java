package com.hackathong5.hackathong5.services;

import com.hackathong5.hackathong5.models.PrestamoModel;
import com.hackathong5.hackathong5.repositories.PrestamoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hackathong5.hackathong5.hackathong5Application;

import java.util.List;
import java.util.Optional;


@Service
public class PrestamoService {

    @Autowired
    PrestamoRepository prestamoRepository;

    public List<PrestamoModel> findAll() {
        System.out.println("findAll en PrestamoService");

        return this.prestamoRepository.findAll();
    }

    public Optional<PrestamoModel> findByIdUsuario(String idUsuario) {
        System.out.println("findByIdUsuario en PrestamoService");

        return this.prestamoRepository.findByIdUsuario(idUsuario);
    }

    public PrestamoModel add(PrestamoModel prestamo) {
        System.out.println("add en PrestamoService");

        return this.prestamoRepository.save(prestamo);
    }

    public Optional<PrestamoModel> findByBody(String idUsuario, String idProducto) {
        System.out.println("findByBody en PrestamoService");

        Optional <PrestamoModel> result = Optional.empty();

        for (PrestamoModel prestamoInList : hackathong5Application.prestamoModels){
            if (prestamoInList.getIdUsuario().equals(idUsuario) &&
                    prestamoInList.getIdProducto().equals(idProducto)){
                System.out.println("Prestamo encontrado");
                result = Optional.of(prestamoInList);
            }
        }

        return result;
    }

    public Optional<PrestamoModel> findByBodyUpdate(String idUsuario, String idProducto) {
        System.out.println("findByBody en PrestamoService");

        Optional <PrestamoModel> result = Optional.empty();

        for (PrestamoModel prestamoInList : hackathong5Application.prestamoModels){
            if (prestamoInList.getIdUsuario().equals(idUsuario) &&
                    prestamoInList.getIdProducto().equals(idProducto)){
                System.out.println("Prestamo encontrado");
                result = Optional.of(prestamoInList);
            }
        }

        return result;
    }

    public boolean delete(String idUsuario, String idProducto) {
        System.out.println("delete en PrestamoService");

        boolean result = false;

        Optional<PrestamoModel> prestamoToDelete = this.findByBody(idUsuario, idProducto);

        if (prestamoToDelete.isPresent() == true) {
            result = true;
            this.prestamoRepository.delete(prestamoToDelete.get());
        }

        return result;

    }

    public boolean update(String idUsuario, String idProducto, float importe) {
        System.out.println("update en PrestamoService");
        System.out.println(importe);

        boolean result = false;

        Optional<PrestamoModel> prestamoToUpdate = this.findByBodyUpdate(idUsuario, idProducto);

        System.out.println(idUsuario + " " + idProducto + " " + importe);

        if (prestamoToUpdate.isPresent() == true) {
            System.out.println("Prestamo está pasando por aquí");
            result = this.prestamoRepository.update(prestamoToUpdate.get(), importe);
        }

        return result;
    }

}
